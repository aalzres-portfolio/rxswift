//
//  CombineInstagramRepository.swift
//  RxSwiftPortfolio
//
//  Created by Andres Felipe Alzate Restrepo on 27/07/2020.
//  Copyright © 2020 Andres Felipe Alzate Restrepo. All rights reserved.
//

import Foundation

protocol CombineInstagramRepositoryOutput: class {
    
}

protocol CombineInstagramRepository {
    
}

class CombineInstagramRepositoryImpl: CombineInstagramRepository {
    weak var output: CombineInstagramRepositoryOutput?
}
