//
//  PColor.swift
//  PORTFOLIO
//
//  Created by Andres Felipe Alzate on 16/06/2020.
//  Copyright © 2020 aalzres. All rights reserved.
//

import UIKit

struct PColor {
    static let white: UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    static let black: UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    static let grayL: UIColor = #colorLiteral(red: 0.8784313725, green: 0.8980392157, blue: 0.9254901961, alpha: 1)
    static let grayM: UIColor = #colorLiteral(red: 0.7607843137, green: 0.7960784314, blue: 0.8509803922, alpha: 1)
    static let grayD: UIColor = #colorLiteral(red: 0.6392156863, green: 0.6941176471, blue: 0.7764705882, alpha: 1)
    
    static let lineColor: UIColor = PColor.grayL
}
