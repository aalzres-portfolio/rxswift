//
//  CombineInstagram.swift
//  RxSwiftPortfolio
//
//  Created by Andres Felipe Alzate Restrepo on 27/07/2020.
//  Copyright © 2020 Andres Felipe Alzate Restrepo. All rights reserved.
//

import Foundation

protocol CombineInstagramOutput: class {
    
}

protocol CombineInstagram {
    
}

class CombineInstagramImpl: CombineInstagram {
    private let repository: CombineInstagramRepository
    
    weak var output: CombineInstagramOutput?
    
    init(repository: CombineInstagramRepository) {
        self.repository = repository
    }
}

extension CombineInstagramImpl: CombineInstagramRepositoryOutput {
    
}
