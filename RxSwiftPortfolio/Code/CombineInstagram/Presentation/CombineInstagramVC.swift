//
//  CombineInstagramVC.swift
//  RxSwiftPortfolio
//
//  Created by Andres Felipe Alzate Restrepo on 27/07/2020.
//  Copyright © 2020 Andres Felipe Alzate Restrepo. All rights reserved.
//

import UIKit

class CombineInstagramVC: UIViewController {
    private let presenter: CombineInstagramPresenter
    
    private lazy var collageView: UIImageView = {
        let collageView = UIImageView()
        collageView.layer.borderColor = PColor.black.cgColor
        collageView.layer.borderWidth = PDimen.componentButtonBorderWidth
        collageView.layer.cornerRadius = PDimen.cornerRadius
        return collageView
    }()
    private lazy var clearButton: UIButton = {
        let clearButton = UIButton()
        clearButton.setTitle("common_clear".localized(), for: .normal)
        clearButton.layer.cornerRadius = Constant.buttonHeight/2
        clearButton.backgroundColor = PColor.grayM
        return clearButton
    }()
    private lazy var saveButton: UIButton = {
        let saveButton = UIButton()
        saveButton.setTitle("common_save".localized(), for: .normal)
        saveButton.layer.cornerRadius = Constant.buttonHeight/2
        saveButton.backgroundColor = PColor.grayD
        return saveButton
    }()
    
    init(presenter: CombineInstagramPresenter) {
        self.presenter = presenter
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configBarButtonItem()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    //MARK: - Setups
    private func setupView() {
        view.backgroundColor = .white
        
        setupCollageView()
        setupButtons()
    }
    
    private func configBarButtonItem() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "common_add".localized(), style: .plain, target: self, action: #selector(goPhoto))
    }
    
    private func setupCollageView() {
        collageView.anchor(view,
                           top: view.topAnchor,
                           paddingTop: view.frame.height * Constant.paddingTopMultiplier,
                           centerX: view.centerXAnchor,
                           widthConstant: view.frame.width * Constant.widthMultiplier,
                           heightConstant: Constant.collageViewHeight)
    }
    
    private func setupButtons() {
        clearButton.anchor(view,
                           top: collageView.bottomAnchor,
                           paddingTop: PDimen.paddingXL,
                           leading: collageView.leadingAnchor,
                           widthConstant: Constant.buttonWidth,
                           heightConstant: Constant.buttonHeight)
        
        saveButton.anchor(view,
                           top: collageView.bottomAnchor,
                           paddingTop: PDimen.paddingXL,
                           trailing: collageView.trailingAnchor,
                           widthConstant: Constant.buttonWidth,
                           heightConstant: Constant.buttonHeight)
    }
}
// MARK: - Selector
extension CombineInstagramVC {
    @objc
    func goPhoto() {
        presenter.goPhoto()
    }
}
// MARK: - CombineInstagramPresenterOutput
extension CombineInstagramVC: CombineInstagramPresenterOutput {}
// MARK: - CombineInstagramPresenterOutput
private struct Constant {
    static let paddingTopMultiplier: CGFloat = 0.2
    static let widthMultiplier: CGFloat = 0.8
    static let collageViewHeight: CGFloat = 150
    
    static let buttonHeight: CGFloat = 100
    static let buttonWidth: CGFloat = buttonHeight
}
